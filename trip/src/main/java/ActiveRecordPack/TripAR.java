package ActiveRecordPack;

import ddbbPack.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TripAR {
    String id,cus_id,veh_id,drv_id;
    
    public List<TripAR> select(TripAR trip){
        List response=new ArrayList<>();
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="SELECT trp_id, cus_id, veh_id, drv_rut\n" +
        "FROM public.tri_trip where trp_id=?;";
        try {
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1, trip.getId());
            ResultSet res=stat.executeQuery();
            while(res.next()){
                TripAR tempTrip=new TripAR();
                tempTrip.setId(res.getString("trp_id"));
                tempTrip.setDrv_id(res.getString("cus_id"));
                tempTrip.setCus_id(res.getString("veh_id"));
                tempTrip.setVeh_id(res.getString("drv_rut"));
                
                response.add(tempTrip);
            }
            c.close();
            return response;
        }catch(SQLException e){
            
        }
        
        return response;
    }
    public List<TripAR> select(){
        List response=new ArrayList<>();
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="SELECT trp_id, cus_id, veh_id, drv_rut\n" +
        "FROM public.tri_trip;";
        try {
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            
            ResultSet res=stat.executeQuery();
            while(res.next()){
                TripAR tempTrip=new TripAR();
                tempTrip.setId(res.getString("trp_id"));
                tempTrip.setDrv_id(res.getString("cus_id"));
                tempTrip.setCus_id(res.getString("veh_id"));
                tempTrip.setVeh_id(res.getString("drv_rut"));
                
                response.add(tempTrip);
            }
            c.close();
            return response;
        }catch(SQLException e){
            
        }
        
        return response;
    }
    public void update(TripAR trip){
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="UPDATE public.tri_trip\n" +
"	SET cus_id=?, veh_id=?, drv_rut=?\n" +
"	WHERE trp_id=?;";
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1,trip.getCus_id());
            stat.setString(2,trip.getVeh_id());
            stat.setString(3,trip.getDrv_id());
            stat.setInt(4, Integer.parseInt( trip.getId() ));
            System.out.println(stat);
            stat.executeQuery();
            c.close();
        }catch(SQLException e){
            System.out.println(e);
        }
    }
    public void insert(TripAR trip){
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="INSERT INTO public.tri_trip(\n" +
"	cus_id, veh_id, drv_rut,trp_id)\n" +
"	VALUES (?, ?, ?, ?);";
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1,trip.getCus_id());
            stat.setString(2,trip.getVeh_id());
            stat.setString(3,trip.getDrv_id());
            stat.setInt(4,Integer.parseInt( trip.getId() ));
            System.out.println(stat);
            stat.executeQuery();
            c.close();
        }catch(SQLException e){
            System.out.println(e);
        }
    }
    public void delete(TripAR trip){
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="DELETE FROM public.tri_trip\n" +
"	WHERE trp_id=?;";
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setInt(1,Integer.parseInt( trip.getId() ));
            stat.executeQuery();
            c.close();
        }catch(SQLException e){
        }
    }
    
    

    //<editor-fold>
    public void setDrv_id(String drv_id) {
        this.drv_id = drv_id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public void setCus_id(String cus_id) {
        this.cus_id = cus_id;
    }

    public void setVeh_id(String veh_id) {
        this.veh_id = veh_id;
    }
    public String getId() {
        return id;
    }

    public String getCus_id() {
        return cus_id;
    }

    public String getVeh_id() {
        return veh_id;
    }

    public String getDrv_id() {
        return drv_id;
    }
    //</editor-fold>
    
}

