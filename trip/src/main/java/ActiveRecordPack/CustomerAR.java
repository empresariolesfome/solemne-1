package ActiveRecordPack;

import ddbbPack.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CustomerAR {
    //SELECT cus_id, cus_name, cus_mail, cus_phone
	//FROM public.cus_customers;
    //int id;//Serial
    String name,mail,phone,id;
    
    public List<CustomerAR> select(CustomerAR customer){
        List response = new ArrayList<>();
        Connect connection=new Connect();
        PreparedStatement stat;
        
        String query="SELECT cus_id, cus_name, cus_mail, cus_phone\n" +
        "FROM public.cus_customers where cus_id=?;";
        System.out.println(customer.getId());
        
        try {
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            
            ResultSet res=stat.executeQuery();
            while(res.next()){
                CustomerAR tempCustomer=new CustomerAR();
                tempCustomer.setId(res.getString("cus_id"));
                tempCustomer.setName(res.getString("cus_name"));
                tempCustomer.setMail(res.getString("cus_mail"));
                tempCustomer.setPhone(res.getString("cus_phone"));
                
                response.add(tempCustomer);
                c.close();
            }
            return response;
        } catch (SQLException e) {
            
        }
        
        return response;
    }
    public List<CustomerAR> select(){
        List response = new ArrayList<>();
        Connect connection=new Connect();
        PreparedStatement stat;
        
        String query="SELECT cus_id, cus_name, cus_mail, cus_phone\n" +
        "FROM public.cus_customers;";
        
        try {
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            
            ResultSet res=stat.executeQuery();
            while(res.next()){
                CustomerAR tempCustomer=new CustomerAR();
                tempCustomer.setId(res.getString("cus_id"));
                tempCustomer.setName(res.getString("cus_name"));
                tempCustomer.setMail(res.getString("cus_mail"));
                tempCustomer.setPhone(res.getString("cus_phone"));
                response.add(tempCustomer);
                c.close();
            }
            return response;
        } catch (SQLException e) {
            
        }
        
        return response;
    }
    
    public boolean update(CustomerAR customer){
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="UPDATE public.cus_customers\n" +
"	SET cus_name=?, cus_mail=?, cus_phone=? \n" +
"	WHERE cus_id=?;";
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1,customer.getName());
            stat.setString(2,customer.getMail());
            stat.setString(3,customer.getPhone());
            stat.setString(4,customer.getId());
            
            stat.executeQuery();
            c.close();
            /*Siempre hay Sqlexception*/
        }catch(SQLException e){
            //System.out.println(e);
        }
        return false;
    }
    public void insert(CustomerAR customer){
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="INSERT INTO public.cus_customers(\n" +
"	cus_id, cus_name, cus_mail, cus_phone)\n" +
"	VALUES (?, ?, ?, ?);";
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1,customer.getId());
            stat.setString(2,customer.getName());
            stat.setString(3,customer.getMail());
            stat.setString(4,customer.getPhone());
            
            
            stat.executeQuery();
            c.close();
            /*Siempre hay Sqlexception*/
        }catch(SQLException e){
            System.out.print(e);
        }   
    }
    public void delete(CustomerAR customer){
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="DELETE FROM public.cus_customers\n" +
"	WHERE cus_id=?;";
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1,customer.getId());
            
            stat.executeQuery();
            /*Siempre hay Sqlexception*/
            c.close();
        }catch(SQLException e){
            
        }
    }
    //<editor-fold>
    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getMail() {
        return mail;
    }

    public String getPhone() {
        return phone;
    }
    //</editor-fold>
}
