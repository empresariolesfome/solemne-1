package ActiveRecordPack;

import ddbbPack.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class VehicleAR {
    String id,model;
    int weight,maxload;//unsigned?
 public List<VehicleAR> select(){
     List response = new ArrayList<>();
     Connect connection=new Connect();
     PreparedStatement stat;
     String query="SELECT veh_id, veh_model, veh_weight, veh_maxload\n" +
"	FROM public.veh_vehicles;";
     try {
         Connection c=connection.init();
         stat=c.prepareStatement(query);
         ResultSet res=stat.executeQuery();
         while(res.next()){
             VehicleAR tempV=new VehicleAR();
             tempV.setId(res.getString("veh_id"));
             tempV.setModel(res.getString("veh_model"));
             tempV.setWeight(res.getInt("veh_weight"));
             tempV.setMaxload(res.getInt("veh_maxload"));
             
             response.add(tempV);
         }
         c.close();
         return response;        
     } catch (SQLException e) {
         System.out.println(e);
     }
     return response;
 }
 public List<VehicleAR> select(VehicleAR vehicle){
     List response = new ArrayList<>();
     Connect connection=new Connect();
     PreparedStatement stat;
     String query="SELECT veh_id, veh_model, veh_weight, veh_maxload\n" +
"	FROM public.veh_vehicles where veh_id=?;";
     try {
         Connection c=connection.init();
         stat=c.prepareStatement(query);
         stat.setString(1, vehicle.getId());
         ResultSet res=stat.executeQuery();
         while(res.next()){
             VehicleAR tempV=new VehicleAR();
             tempV.setId(res.getString("veh_id"));
             tempV.setModel(res.getString("veh_model"));
             tempV.setWeight(res.getInt("veh_weight"));
             tempV.setMaxload(res.getInt("veh_maxload"));
             
             response.add(tempV);
         }
         c.close();
         return response;        
     } catch (SQLException e) {
         System.out.println(e);
     }
     return response;
 }
 public boolean update(VehicleAR vehicle){
     Connect connection=new Connect();
     PreparedStatement stat;
     String query="UPDATE public.veh_vehicles\n" +
             "SET veh_model=?, veh_weight=?, veh_maxload=?\n" +
             " WHERE veh_id=?;";
     try{
         Connection c=connection.init();
         stat=c.prepareStatement(query);
         stat.setString(1, vehicle.getModel());
         stat.setInt(2, vehicle.getWeight());
         stat.setInt(3, vehicle.getMaxload());
         stat.setString(4, vehicle.getId());
         stat.executeQuery();
         c.close();
         return stat.getUpdateCount()==0;
     }catch(SQLException e){
         return false;
     }
 }
 public boolean insert(VehicleAR vehicle){
     Connect connection=new Connect();
     PreparedStatement stat;
     String query="INSERT INTO public.veh_vehicles(\n" +
             "veh_id, veh_model, veh_weight, veh_maxload)\n" +
             "VALUES (?, ?, ?, ?);";
     try{
         Connection c=connection.init();
         stat=c.prepareStatement(query);
         stat.setString(1, vehicle.getId());
         stat.setString(2, vehicle.getModel());
         stat.setInt(3, vehicle.getWeight());
         stat.setInt(4, vehicle.getMaxload());
         
         stat.executeQuery();
         c.close();
         return stat.getUpdateCount()==0;
     }catch(SQLException e){
     }
     return false;
 }
 
 public void delete(VehicleAR vehicle){
     Connect connection=new Connect();
     PreparedStatement stat;
     String query="DELETE FROM public.veh_vehicles\n" +
"	WHERE veh_id=?;";
     try{
         Connection c=connection.init();
         stat=c.prepareStatement(query);
         stat.setString(1, vehicle.getId());
         
         System.out.println(stat);
         stat.executeQuery();
         c.close();
     }catch(SQLException e){
         System.out.println(e);
     }
     
 }
 
    //<editor-fold>
    public String getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public int getWeight() {
        return weight;
    }

    public int getMaxload() {
        return maxload;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setMaxload(int maxload) {
        this.maxload = maxload;
    }
    //</editor-fold>
 
 
}
