package ActiveRecordPack;

import ddbbPack.Connect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DriverAR {
    String id,vehId,name,lastName;
    //
    public List<DriverAR> select(){
        List response = new  ArrayList<>();
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="SELECT drv_rut, drv_name, drv_last_name, veh_id FROM public.drv_drivers;";
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            ResultSet res=stat.executeQuery();
            while(res.next()){
                DriverAR tempDriver=new DriverAR();
                tempDriver.setId(res.getString("drv_rut"));
                tempDriver.setName(res.getString("drv_name"));
                tempDriver.setLastName(res.getString("drv_last_name"));
                tempDriver.setVehId(res.getString("veh_id"));
                
                response.add(tempDriver);        
                c.close();
            }
            return response;
        }catch(SQLException e){
            System.out.println(e);
        }
        //return null;
        return response;
    }
    public List<DriverAR> select(DriverAR driver){
        List response = new  ArrayList<>();
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="SELECT drv_rut, drv_name, drv_last_name, veh_id FROM public.drv_drivers where drv_rut=?;";
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1, driver.getId());
            ResultSet res=stat.executeQuery();
            while(res.next()){
                DriverAR tempDriver=new DriverAR();
                tempDriver.setId(res.getString("drv_rut"));
                tempDriver.setName(res.getString("drv_name"));
                tempDriver.setLastName(res.getString("drv_last_name"));
                tempDriver.setVehId(res.getString("veh_id"));
                //System.out.println(tempDriver.getName());
                response.add(tempDriver);        
            }
            c.close();
            return response;
        }catch(SQLException e){
            System.out.println(e);
        }
        
        return response;
    }
    public boolean insert(DriverAR driver){
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="INSERT INTO public.drv_drivers(\n" +
"	drv_name, drv_last_name, veh_id, drv_rut)\n" +
"	VALUES (?, ?, ?, ?);";
        
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1, driver.getName());
            stat.setString(2, driver.getLastName());
            stat.setString(3, driver.getVehId());
            stat.setString(4, driver.getId());
            System.out.println(stat);
            stat.executeQuery();
            
            System.out.println(stat.getUpdateCount()+" rows has updated");
            c.close();
            //return stat.getUpdateCount()==0;
            return true;
        }catch(SQLException e){
            System.out.println(e.getSQLState());
            if(e.getSQLState()=="23505"){
                return false;
            }
            return false;
            //23505	unique_violation
        }
        //return false;
    }
    public String delete(DriverAR driver){
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="DELETE FROM public.drv_drivers\n" +
        "where drv_rut=?";
        try{
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1, driver.getId());
            stat.executeQuery();
            int updated=stat.getUpdateCount();            
            System.out.println(updated+" rows has updated");
            c.close();
            return "ok";
        }catch(SQLException e){
            if(e.getSQLState()=="02000"){
                return "ok";
            }
            return e.getSQLState();
        }
        
    }
    public boolean update(DriverAR driver){
        Connect connection=new Connect();
        PreparedStatement stat;
        String query="UPDATE public.drv_drivers\n" +
        "SET drv_name=?, drv_last_name=?, veh_id=?\n" +
        "WHERE drv_rut=?;";
        try{            
            Connection c=connection.init();
            stat=c.prepareStatement(query);
            stat.setString(1, driver.getName());
            stat.setString(2, driver.getLastName());
            stat.setString(3, driver.getVehId());
            stat.setString(4, driver.getId());
            //System.out.println(stat);
            stat.executeQuery();
            
            int updated=stat.getUpdateCount();            
            System.out.println(updated+" rows has updated");
            c.close();
            return updated==0;
        }catch(SQLException e){
            System.out.println(e);
        }
        return false;
    }
    //
    //

    //<editor-fold>
    public String getId() {
        return id;
    }

    public String getVehId() {
        return vehId;
    }
    
    
    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setVehId(String vehId) {
        this.vehId = vehId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    //</editor-fold>
}
