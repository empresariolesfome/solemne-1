package ddbbPack;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connect {
    Connection con=null;
    String db="ddbbpg_trips";
    String driverDB="org.postgresql.Driver";
    String port="5432";
    String host="localhost";
    String user="postgres";
    String pass="root";
    String url="jdbc:postgresql://"+host+":"+port+"/"+db;
    public Connection init() throws SQLException{
        try {
            Class.forName(driverDB);
            con=DriverManager.getConnection(url,user,pass);
            return con;
        } catch (Exception e) {
            System.out.println("no");
            System.out.println(e);
            return null;
            //https://www.postgresql.org/docs/9.2/static/errcodes-appendix.html
        }
        
    }
}
