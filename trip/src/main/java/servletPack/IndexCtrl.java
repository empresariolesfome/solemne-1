package servletPack;

import ActiveRecordPack.CustomerAR;
import ActiveRecordPack.DriverAR;
import ActiveRecordPack.VehicleAR;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IndexCtrl extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        
        
        
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String href;
        href=request.getParameter("href");
        if("drivers".equals(href)){
            VehicleAR vehicle=new VehicleAR();
            List<VehicleAR> vList=new ArrayList<>();
            vList=vehicle.select();
            System.out.println("Vlist: "+vList);
            if(vList.isEmpty()){
                response.getWriter().print("<script>var a=function(){alert( 'No existen vehículos' )};a();</script>");
            }else{

            }
            request.setAttribute("vehicles", vList);
            request.getRequestDispatcher("/driver/").forward(request, response);
        }
        if("trips".equals(href)){
            System.out.println("trips");
            VehicleAR vehicles=new VehicleAR();
            List<VehicleAR> vList=new ArrayList<>();
            vList=vehicles.select();
            
            DriverAR drivers=new DriverAR();
            List<DriverAR> dList=new ArrayList<>();
            dList=drivers.select();
            
            CustomerAR customers=new CustomerAR();
            List<CustomerAR> cList=new ArrayList<>();
            cList=customers.select();
            
            request.setAttribute("vehicles", vList);
            request.setAttribute("drivers", dList);
            request.setAttribute("customers", cList);
            
            request.getRequestDispatcher("/trip/").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    

}
