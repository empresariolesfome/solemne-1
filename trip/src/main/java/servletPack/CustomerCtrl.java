/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servletPack;

import ActiveRecordPack.CustomerAR;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author camilo
 */
public class CustomerCtrl extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        System.out.println("GET");
        String id=request.getParameter("id");
        if(id!=""){
            CustomerAR customer=new CustomerAR();
            customer.setId( id );
            List<CustomerAR> customerList=new ArrayList<>();
            customerList=customer.select(customer);
            request.setAttribute("customers",customerList);
        }else{
            CustomerAR customer=new CustomerAR();
            List<CustomerAR> customerList=new ArrayList<>();
            customerList=customer.select();
            request.setAttribute("customers", customerList);
        }
        request.getRequestDispatcher("customer/select.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        String action=request.getParameter("act");
        System.out.println(action);
        
        switch(action){
            case "create":{
                create(request,response);
                break;
            }
            case "delete":{
                delete(request,response);
                break;
            }
            case "update":{
                update(request,response);
            }
        }
    }
    private void create(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        CustomerAR cus=new CustomerAR();
        cus.setId( request.getParameter("id") );
        cus.setName( request.getParameter("name") );
        cus.setMail( request.getParameter("mail") );
        cus.setPhone(request.getParameter("phone") );
        cus.insert(cus);
        
        response.getWriter().print("<script>  alert( 'Registro enviado' );window.history.back()  </script>");
    }
    private void delete(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        CustomerAR cus=new CustomerAR();
        cus.setId( request.getParameter("id") );
        cus.delete(cus);
        response.getWriter().print("<script>  alert( 'Registro borrado' );window.history.back()  </script>");
    }
    private void update(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        CustomerAR cus=new CustomerAR();
        cus.setId( request.getParameter("id") );
        cus.setName( request.getParameter("name") );
        cus.setMail( request.getParameter("mail") );
        cus.setPhone(request.getParameter("phone") );
        cus.update(cus);
        response.getWriter().print("<script>  alert( 'Actualización enviada' );window.history.back()  </script>");
    }

}
