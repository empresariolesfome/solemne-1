package servletPack;

import ActiveRecordPack.DriverAR;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Driverctrl extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String id=request.getParameter("id");
        //System.out.println("id: "+id);
        
        if(id!=""){
            DriverAR driver=new DriverAR();
            driver.setId(id);
            List<DriverAR> driverList=new ArrayList<>();
            driverList=driver.select(driver);
            //request.setAttribute("driver", driver);
            request.setAttribute("drivers", driverList);
            /*if(driverList.isEmpty()){
                request.setAttribute("name", "sin resultados");
            }else{}*/
        }else{
            DriverAR driver=new DriverAR();
            List<DriverAR> driversList=new ArrayList<>();
            driversList=driver.select();
            request.setAttribute("drivers", driversList);
            /*if(driversList.isEmpty() ){
                
            }else{
                request.setAttribute("drivers", driversList);
            }*/
        }
        request.getRequestDispatcher("driver/select.jsp").forward(request, response);
        
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        response.setContentType("text/html;charset=UTF-8");
        System.out.println("Post");
        String action=request.getParameter("act");
        switch (action){
            case "create": {
                System.out.println("create");
                DriverAR driver=new DriverAR();
                driver.setId(request.getParameter("id"));
                driver.setName( request.getParameter("name") );
                driver.setLastName(request.getParameter("lastname") );
                driver.setVehId(request.getParameter("vehid") );
                //System.out.println(driver.getVehId());
                driver.insert(driver);
                /*if(driver.insert(driver)==true){
                    response.getWriter().print("<script>  alert( 'Registro enviado' );window.history.back()  </script>");
                }else{
                    response.getWriter().print("<script>  alert( 'No se ha podido enviar información' );window.history.back()  </script>");
                }*/
                response.getWriter().print("<script>  alert( 'Información enviada' );window.history.back()  </script>");
                
                
            };break;
            case "delete": {
                DriverAR driver=new DriverAR();
                driver.setId( request.getParameter("id") );
                String res=driver.delete(driver);
                System.out.println(res);
                if(res=="ok"){
                    response.getWriter().print("<script>  alert( 'Registro borrado' );window.history.back()  </script>");
                }else{
                    response.getWriter().print("<script>  alert( 'Error +' "+res+" );window.history.back()  </script>");
                }
                
            };break;
            case "update": {
                DriverAR driver=new DriverAR();
                driver.setId(request.getParameter("id"));
                driver.setName( request.getParameter("name") );
                driver.setLastName(request.getParameter("lastname") );
                driver.setVehId(request.getParameter("vehid") );
                
                if(driver.update(driver)){
                    response.getWriter().print("<script>  alert( 'Registro actualizado' );window.history.back()  </script>");
                }else{
                    response.getWriter().print("<script>  alert( 'Información enviada' );window.history.back()  </script>");
                }
                
            };break;
        }
    }

}
