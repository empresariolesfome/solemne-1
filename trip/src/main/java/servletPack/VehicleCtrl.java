package servletPack;

import ActiveRecordPack.VehicleAR;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class VehicleCtrl extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String id=request.getParameter("id");
        
        if(id!=""){
            VehicleAR vehicle=new VehicleAR();
            List<VehicleAR> vList=new ArrayList<>();
            vList=vehicle.select();
            request.setAttribute("vehicles", vList);
            if(vList.isEmpty()){
                response.getWriter().print("<script>  alert( 'No se ha encontrado vehículos' );window.history.back()  </script>");    
            }else{
                request.getRequestDispatcher("vehicle/select.jsp").forward(request, response);
            }
        }else{
            VehicleAR vehicle=new VehicleAR();
            List<VehicleAR> vList=new ArrayList<>();
            vList=vehicle.select();
            request.setAttribute("vehicles", vList);
            if(vList.isEmpty()){
                response.getWriter().print("<script>  alert( 'No se ha encontrado vehículos' );window.history.back()  </script>");    
            }else{
                request.getRequestDispatcher("vehicle/select.jsp").forward(request, response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String action=request.getParameter("act");
        System.out.println(action);
        switch(action){
            case "create":{
                create(request,response);
                break;
            }
            case "delete":{
                delete(request,response);
                break;
            }
            case "update":{
                update(request,response);
            }
        }
    }
    private void create(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        VehicleAR v=new VehicleAR();
        v.setId(request.getParameter("id"));
        v.setModel(request.getParameter("model"));
        v.setWeight( Integer.parseInt( request.getParameter("weight") ) );
        v.setMaxload(Integer.parseInt( request.getParameter("maxload") ) );
        v.insert(v);
        /*if(v.insert(v)){Siempre devuelve false
            response.getWriter().print("<script>  alert( 'Registro añadido' );window.history.back()  </script>");
        }else{
            response.getWriter().print("<script>  alert( 'No se ha podido añadir información' );window.history.back()  </script>");
        }*/
        response.getWriter().print("<script>  alert( 'Registro enviado' );window.history.back()  </script>");
    }
    private void update(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        VehicleAR v=new VehicleAR();
        v.setId(request.getParameter("id"));
        v.setModel(request.getParameter("model"));
        v.setWeight( Integer.parseInt( request.getParameter("weight") ) );
        v.setMaxload(Integer.parseInt( request.getParameter("maxload") ) );
        v.update(v);//mismo caso, siempre devuelve false;
        response.getWriter().print("<script>  alert( 'Actualización enviada' );window.history.back()  </script>");
    }
    private void delete(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        VehicleAR v=new VehicleAR();
        v.setId(request.getParameter("id"));
        v.delete(v);
        
        response.getWriter().print("<script>  alert( 'Registro eliminado' );window.history.back()  </script>");
    }

}
