package servletPack;

import ActiveRecordPack.TripAR;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TripCtrl extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        System.out.println("GET");
        String id=request.getParameter("id");
        if(id!=""){
            TripAR trip=new TripAR();
            trip.setId( id );
            List<TripAR> tripList=new ArrayList<>();
            tripList=trip.select(trip);
            request.setAttribute("trips",tripList);
        }else{
            TripAR trip=new TripAR();
            List<TripAR> tripList=new ArrayList<>();
            tripList=trip.select();
            request.setAttribute("trips", tripList);
        }
        request.getRequestDispatcher("/trip/select.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String action=request.getParameter("act");
        
        switch(action){
            case "create":{
                create(request,response);
                break;
            }
            case "delete":{
                delete(request,response);
                break;
            }
            case "update":{
                update(request,response);
            }
        }
    }
    private void create(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        System.out.println("create");
        TripAR trip=new TripAR();
        trip.setId( request.getParameter("id") );
        trip.setCus_id( request.getParameter("customer") );
        trip.setDrv_id( request.getParameter("driver") );
        trip.setVeh_id( request.getParameter("vehicle") );
        
        System.out.println(trip.getCus_id());
        System.out.println(trip.getVeh_id());
        System.out.println(trip.getDrv_id());
        System.out.println(trip.getId());
        
        trip.insert(trip);
        
        response.getWriter().print("<script>  alert( 'Registro enviado' );window.history.back()  </script>");
    }
    private void delete(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        System.out.println("delete");
        TripAR trip=new TripAR();
        trip.setId( request.getParameter("id") );
        
        trip.delete(trip);
        response.getWriter().print("<script>  alert( 'Registro borrado' );window.history.back()  </script>");
    }
    private void update(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
        TripAR trip=new TripAR();
        trip.setId( request.getParameter("id") );
        trip.setCus_id( request.getParameter("customer") );
        trip.setDrv_id( request.getParameter("driver") );
        trip.setVeh_id( request.getParameter("vehicle") );
        
        trip.update(trip);
        response.getWriter().print("<script>  alert( 'Actualización enviada' );window.history.back()  </script>");
        
        System.out.println("update");
    }
}
