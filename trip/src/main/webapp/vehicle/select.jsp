<%-- 
    Document   : select
    Created on : 02-10-2017, 18:41:22
    Author     : camilo
--%>

<%@page import="ActiveRecordPack.VehicleAR"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            td,th{
                border:1px solid black;
                padding: .3em;
            }
        </style>
    </head>
    <body>
        <h1>Vehicles</h1>
        <table>
            <tr>
                <td>Patente</td>
                <td>Modelo</td>
                <td>Peso</td>
                <td>Carga máxima</td>
            </tr>
            <%
                ArrayList<VehicleAR> vehicleList= (ArrayList<VehicleAR>)request.getAttribute("vehicles");
                for(int i=0;i<vehicleList.size();i++){
                    VehicleAR temp=vehicleList.get(i);
                    %>
                    <tr>
                        <td><%= temp.getId() %></td>
                        <td><%= temp.getModel()%></td>
                        <td><%= temp.getWeight()%></td>
                        <td><%= temp.getMaxload()%></td>
                    </tr>
                    <%
                }                
            %>
            
        </table>
    </body>
</html>
