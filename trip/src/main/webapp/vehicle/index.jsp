<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello vehicles!</h1>
        <fieldset>
            <legend>Buscar</legend>
            <form method="GET" action="../VehicleCtrl">
                <input name="id" placeholder="Patente">
                <br>
                <input type="submit" value="Buscar">
            </form>
        </fieldset>
        <fieldset>
            <legend>Agregar</legend>
            <form method="POST" action="../VehicleCtrl">
                <input type="hidden" name="act" value="create">
            
                <input name="id" placeholder="Ingrese patente">
                <br>
                <input name="model" placeholder="Ingrese modelo">
                <br>
                <input name="weight" type="number" placeholder="Ingrese peso">
                <br>
                <input name="maxload" type="number" placeholder="Ingrese carga máxima">
                <br>            
                <input type="submit" value="Agregar">
            </form>
        </fieldset>
        <fieldset>
            <legend>Actualizar</legend>
            <form method="POST" action="../VehicleCtrl">
                <input type="hidden" name="act" value="update">
            
                <input name="id" placeholder="Ingrese patente">
                <br>
                <input name="model" placeholder="Ingrese modelo">
                <br>
                <input name="weight" type="number" placeholder="Ingrese peso">
                <br>
                <input name="maxload" type="number" placeholder="Ingrese carga máxima">
                <br>            
                <input type="submit" value="Agregar">
            </form>
        </fieldset>
        <fieldset>
            <legend>Borrar</legend>
            <form method="POST" action="../VehicleCtrl">
                <input type="hidden" name="act" value="delete">
            
                <input name="id" placeholder="Ingrese patente">
                <br>
                
                <input type="submit" value="Borrar">
            </form>
        </fieldset>
    </body>
</html>
