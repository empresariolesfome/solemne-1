<%@page import="ActiveRecordPack.VehicleAR"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        
        
        <fieldset>
            <legend>Buscar</legend>
            <form method="GET" action="Driverctrl">
                <input type="number" name="id" placeholder="id">
                <br>
                <input type="submit" value="Buscar">
            </form>
        </fieldset>
        <br>
        <fieldset>
            <legend>Agregar</legend>
            <form method="POST" action="./Driverctrl">
                <input type="hidden" name="act" value="create">
                
                <input name="id" placeholder="Ingrese rut">
                <br>
                <input name="name" placeholder="Ingrese nombre">
                <br>
                <input name="lastname" placeholder="Ingrese apellido">
                <br>
                <!--<input name="vehid" placeholder="Ingrese patente vehículo">-->
                <select name="vehid">
                    <%
                        ArrayList<VehicleAR> vList=(ArrayList<VehicleAR>) request.getAttribute("vehicles");
                        for(int idx=0;idx<vList.size();idx++){
                            VehicleAR v=vList.get(idx);
                            String id=v.getId();
                            %>
                            <option value="<%=id%>"><%=id%></option>
                            <%
                        }
                    %>
                    
                </select>
                <br>
                <input type="submit" value="Agregar">
            </form>
        </fieldset>
        <br>
        <fieldset>
            <legend>Borrar</legend>
            <form method="POST" action="./Driverctrl">
                <input type="hidden" name="act" value="delete">
                <input  name="id" placeholder="id del conductor">
                <br>                                
                <input type="submit" value="Borrar">
            </form>
        </fieldset>
        <br>
        <fieldset>
            <legend>Actualizar</legend>
            <form method="POST" action="./Driverctrl">
                <input type="hidden" name="act" value="update">
                
                <input name="id" placeholder="Ingrese rut">
                <br>
                <input name="name" placeholder="Ingrese nombre">
                <br>
                <input name="lastname" placeholder="Ingrese apellido">
                <br>
                <select name="vehid">
                    <%
                        //ArrayList<VehicleAR> vList=(ArrayList<VehicleAR>) request.getAttribute("vehicles");
                        for(int idx=0;idx<vList.size();idx++){
                            VehicleAR v=vList.get(idx);
                            String id=v.getId();
                            %>
                            <option value="<%=id%>"><%=id%></option>
                            <%
                        }
                    %>
                    
                </select>
                <br>
                <input type="submit" value="Actualizar">
            </form>
        </fieldset>
        
    </body>
</html>
