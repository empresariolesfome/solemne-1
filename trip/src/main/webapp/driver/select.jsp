<%-- 
    Document   : select
    Created on : 02-10-2017, 1:40:33
    Author     : camilo
--%>

<%@page import="ActiveRecordPack.DriverAR"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            td,th{
                border:1px solid black;
                padding: .3em;
            }
        </style>
    </head>
    <body>
        <h1>Conductores</h1>
        <table>
            <tr>
                <td>Rut</td>
                <td>Nombre</td>
                <td>Apellido</td>
                <td>Patente vehículo</td>
            </tr>
            <%
                ArrayList<DriverAR> driverList=(ArrayList<DriverAR>)request.getAttribute("drivers");
                if(driverList.isEmpty()){
                    %>
                    <tr>
                        <td>Vacío</td>
                    </tr>
                    <%
                }else{
                    for(int idx=0;idx<driverList.size();idx++){
                        DriverAR temp=driverList.get(idx);
                        %>
                        <tr>
                            <td><%=temp.getId()%></td>
                            <td><%=temp.getName()%></td>
                            <td><%=temp.getLastName()%></td>
                            <td><%=temp.getVehId()%></td>
                        </tr>
                        <%
                    }
                    
                }
            %>
            
        </table>
    </body>
</html>
