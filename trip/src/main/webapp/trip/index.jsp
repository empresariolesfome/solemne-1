
<%@page import="ActiveRecordPack.CustomerAR"%>
<%@page import="ActiveRecordPack.VehicleAR"%>
<%@page import="ActiveRecordPack.DriverAR"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello Trips!</h1>
        <fieldset>
            <legend>Buscar</legend>
            <form method="GET" action="TripCtrl">
                <input type="number" name="id" placeholder="id">
                <br>
                <input type="submit" value="Buscar">
            </form>
        </fieldset>
        <fieldset>
            <legend>Agregar</legend>
            <form  method="POST" action="TripCtrl">
                <input type="hidden" name="act" value="create">
                <input name="id" placeholder="Id"><br>
                
                <select name="customer">
                    <%
                        ArrayList<CustomerAR> cList=(ArrayList<CustomerAR>) request.getAttribute("customers");
                        for(int i=0;i<cList.size();i++){
                            CustomerAR c=cList.get(i);%>
                            <option value="<%= c.getId() %>">
                                <%= c.getId() %> : <%= c.getName()%> Mail: <%= c.getMail()%> Phone: <%= c.getPhone()%>
                            </option><%
                        }
                    %>
                </select>Customer ID / Customer model<br>
                
                <select name="vehicle">
                    <%
                        ArrayList<VehicleAR> vList=(ArrayList<VehicleAR>) request.getAttribute("vehicles");
                        for(int i=0;i<vList.size();i++){
                            VehicleAR v=vList.get(i);%>
                            <option value="<%= v.getId() %>">
                                <%= v.getId() %> : <%= v.getModel()%> Carga máxima: <%= v.getMaxload()%>
                            </option><%
                        }
                    %>
                </select>Vehicle ID / Vehicle model<br>
                
                <select name="driver">
                    <%
                        ArrayList<DriverAR> dList=(ArrayList<DriverAR>) request.getAttribute("drivers");
                        for(int i=0;i<dList.size();i++){
                            DriverAR d=dList.get(i);
                            %>
                            <option value="<%= d.getId() %>">
                                <%= d.getId() %>:<%= d.getName()%>
                            </option><%
                        }
                    %>
                </select>Driver ID / Driver name <br>
                
                
                
                
                <input type="submit">
            </form>
        </fieldset>
        <fieldset>
            <legend>Borrar</legend>
            <form method="POST" action="TripCtrl">
                <input type="hidden" name="act" value="delete">
                <input name="id" placeholder="Id"><br>
                <input type="submit">
            </form>
        </fieldset>
        <fieldset>
            <legend>Actualizar</legend>
            <form method="POST" action="TripCtrl">
                <input type="hidden" name="act" value="update">
                <input name="id" placeholder="Id"><br>
                
                <select name="customer">
                    <%
                        //ArrayList<CustomerAR> cList=(ArrayList<CustomerAR>) request.getAttribute("customers");
                        for(int i=0;i<cList.size();i++){
                            CustomerAR c=cList.get(i);%>
                            <option value="<%= c.getId() %>">
                                <%= c.getId() %> : <%= c.getName()%> Mail: <%= c.getMail()%> Phone: <%= c.getPhone()%>
                            </option><%
                        }
                    %>
                </select>Customer ID / Customer model<br>
                
                <select name="vehicle">
                    <%
                        //ArrayList<VehicleAR> vList=(ArrayList<VehicleAR>) request.getAttribute("vehicles");
                        for(int i=0;i<vList.size();i++){
                            VehicleAR v=vList.get(i);%>
                            <option value="<%= v.getId() %>">
                                <%= v.getId() %> : <%= v.getModel()%> Carga máxima: <%= v.getMaxload()%>
                            </option><%
                        }
                    %>
                </select>Vehicle ID / Vehicle model<br>
                
                <select name="driver">
                    <%
                        //ArrayList<DriverAR> dList=(ArrayList<DriverAR>) request.getAttribute("drivers");
                        for(int i=0;i<dList.size();i++){
                            DriverAR d=dList.get(i);
                            %>
                            <option value="<%= d.getId() %>">
                                <%= d.getId() %>:<%= d.getName()%>
                            </option><%
                        }
                    %>
                </select>Driver ID / Driver name <br>
                
                <input type="submit">
            </form>
        </fieldset>
    </body>
</html>
