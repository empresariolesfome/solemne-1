<%-- 
    Document   : select
    Created on : 04-10-2017, 15:09:31
    Author     : camilo
--%>

<%@page import="ActiveRecordPack.TripAR"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            td,th{
                border:1px solid black;
                padding: .3em;
            }
        </style>
    </head>
    <body>
        <h1>Viajes</h1>
        <table>
            <tr>
                <th>Trip ID</th>
                <th>Driver ID</th>
                <th>Vechicle ID</th>
                <th>Customer ID</th>
            </tr>
            <%
                ArrayList<TripAR> tripList=(ArrayList<TripAR>)request.getAttribute("trips");
                for(int i=0;i<tripList.size();i++){
                    TripAR temp=tripList.get(i);%>
                    <tr> 
                        <td><%= temp.getId() %> </td>
                        <td><%= temp.getDrv_id()%></td>
                        <td><%= temp.getVeh_id()%></td>
                        <td><%= temp.getCus_id()%></td>
                    </tr><%
                }
                if(tripList.isEmpty()){
                    %>
                    <tr>
                        <td>Vacío</td>
                    </tr>
                    <%
                }
            %>
        </table>
    </body>
</html>
