<%-- 
    Document   : index
    Created on : 03-10-2017, 12:02:24
    Author     : camilo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello Customers</h1>
        
        <fieldset>
            <legend>Buscar</legend>
            <form method="GET" action="../CustomerCtrl">
                <input type="number" name="id" placeholder="ID Cliente">
                <br>
                <input type="submit" value="Buscar">
            </form>
        </fieldset>
        <fieldset>
            <legend>Agregar</legend>
            <form method="POST" action="../CustomerCtrl">
                <input type="hidden" name="act" value="create">
                <input  name="id" placeholder="id del cliente"><br>
                <input name="name" placeholder="Nombre del cliente"><br>
                <input name="mail" placeholder="Correo del cliente"><br>
                <input name="phone" placeholder="Teléfono del cliente"><br>
                
                <input type="submit" value="Agregar">
            </form>
        </fieldset>
        <fieldset>
            <legend>Borrar</legend>
            <form method="POST" action="../CustomerCtrl">
                <input type="hidden" name="act" value="delete">
                <input  name="id" placeholder="id del cliente">
                <br>                                
                <input type="submit" value="Borrar">
            </form>
        </fieldset>
        <fieldset>
            <legend>Actualizar</legend>
            <form method ="POST" action="../CustomerCtrl">
                <input type="hidden" name="act" value="update">
                <input  name="id" placeholder="id del cliente"><br>
                <input name="name" placeholder="Nombre del cliente"><br>
                <input name="mail" placeholder="Correo del cliente"><br>
                <input name="phone" placeholder="Teléfono del cliente"><br>
                
                <input type="submit" value="Actualizar">
            </form>
        </fieldset>
    </body>
</html>
