<%-- 
    Document   : select
    Created on : 03-10-2017, 12:02:32
    Author     : camilo
--%>

<%@page import="ActiveRecordPack.CustomerAR"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            td,th{
                border:1px solid black;
                padding: .3em;
            }
        </style>
    </head>
    <body>
        <h1>Clientes</h1>
        <table>
            <tr>
                <td>ID</td>
                <td>Nombre</td>
                <td>Correo</td>
                <td>Teléfono</td>
            </tr>
            <%
                ArrayList<CustomerAR> customerList=(ArrayList<CustomerAR>) request.getAttribute("customers");
                if(customerList.isEmpty()){
                    %>
                    <tr>
                        <td>Vacío</td>
                    </tr>
                    <%
                }else{
                    for(int i=0;i<customerList.size();i++){
                        CustomerAR tempC=customerList.get(i);
                        %>
                        <tr>
                            <td><%=tempC.getId()%></td>
                            <td><%=tempC.getName()%></td>
                            <td><%=tempC.getMail()%></td>
                            <td><%=tempC.getPhone()%></td>
                        </tr>
                        <%
                        }
                    }
                %>
        </table>
    </body>
</html>
